package com.pingpang.minio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.minio.MinioClient;

@Configuration
public class MinioConfig {

	@Value("${minio.endpoint}")
	private String endpoint;
	@Value("${minio.accessKey}")
	private String accessKey;
	@Value("${minio.secretKey}")
	private String secretKey;

	/**
	 * 注入minio 客户端
	 * 
	 * @return
	 */
	@Bean
	public MinioClient minioClient() {
		//http
		//return MinioClient.builder().endpoint(endpoint).credentials(accessKey, secretKey).build();
		//https://192.168.1.100:9000
//		String ip=this.endpoint.split(":")[1];
//		       ip=ip.substring(ip.lastIndexOf("/"));
		String port=this.endpoint.split(":")[2];
		return MinioClient.builder().endpoint(endpoint,Integer.valueOf(port),true).credentials(accessKey, secretKey).build();
	}
}
